import React from 'react';
import './App.css'
//import Button from './components/Button/Button.js';
import Todo from './Todo.js';

const App = () => {
    return(
        <div className="App">
            <Todo/>
        </div>
    )
}

export default App;