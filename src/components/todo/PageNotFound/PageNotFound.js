import React, {Component} from 'react';


export default class PageNotFound extends Component {
    render() {
        return (
            <div>
                Page Was Not Found Please Contact Support at 1-800-123-4567
            </div>
        )
    }
}