import axios from 'axios';
import { API_URL } from '../../Constants/Constants';

class HelloWorldService {


    executeHelloWorldBeanService() {
        return axios.get(`${API_URL}/hello-world-bean`);
    }
    executeHelloWorldService() {
        return axios.get(`${API_URL}/hello-world`);
    }
    executeHelloWorldPathVariableService(name) {
        return axios.get(`${API_URL}/hello-world/path-variable/${name}`);
    }
}

export default new HelloWorldService();