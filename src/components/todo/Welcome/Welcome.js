import React, {useState} from 'react';
import { Link, useParams } from 'react-router-dom';
import HelloWorldService from '../apis/todo/HelloWorldService';

const Welcome = () => {
    const { name } = useParams();
    const [welcomeMessage, setWelcomeMessage] = useState('')

    const retrieveWelcomeMessage = () => {
        HelloWorldService.executeHelloWorldPathVariableService(name)
        .then((r) => handleSuccessfulResponse(r))
        .catch((e) => {
            handleError(e);
        })
    }

    const handleSuccessfulResponse = (r) => {
        setWelcomeMessage(r.data.message);
    }

    const handleError = (error) => {
        console.log(error.response);
        let errorMessage = '';

        if(error.message){
            errorMessage += error.message;
        }
        if(error.response && error.response.data){
            errorMessage += error.response.data.message
        }

        setWelcomeMessage(errorMessage);
    }

    return (
        <div>
            <h1>Welcome</h1>
            <div className='container'>
                Welcome {name}, You can manage your todos <Link to='/todos'>here</Link>
            </div>
            <div className='container'>
                Click here to get a customized welcome message.
                <button className='btn btn-success' 
                    onClick={retrieveWelcomeMessage}>Get Welcome Message</button>
            </div>
            <div className='container'>
                {welcomeMessage}
            </div>
        </div>
    )
}

export default Welcome;