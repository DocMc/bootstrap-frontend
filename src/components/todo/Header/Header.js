
import { NavLink } from 'react-router-dom';
import AuthenticationService from '../AuthenticationService/AuthenticationService';
import { useAuthContext } from '../../../hooks/useAuthContext/useAuthContext';

const Header = () => {
    const {logout} = AuthenticationService();
    const { user } = useAuthContext();
    return (
        <header>
            <nav className='navbar navbar-expand-md navbar-dark bg-dark'>
                <div><a className='navbar-brand' href='https://website-two-mauve.vercel.app/'>DocMc</a></div>
                <ul className='navbar-nav'>
                    {user && <li><NavLink className='nav-link' to='/welcome/DocMc'>Home</NavLink></li>}
                    {user && <li><NavLink className='nav-link' to='/todos'>Todos</NavLink></li>}
                </ul>
                <ul className='navbar-nav navbar-collapse justify-content-end'>
                    {!user && <li><NavLink className='nav-link' to='/login'>Login</NavLink></li>}
                    {user && <li><NavLink className='nav-link' to='/logout' onClick={logout}>Logout</NavLink></li>}
                </ul>
            </nav>
        </header>
    )
}

export default Header;