import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const Logout = () => {
    const nav = useNavigate();
    useEffect(() => {
        setTimeout(() => {
            nav('/login');
        }, 1000);
    }, [nav])

    return (
        <div>
            <h1>You are logged out</h1>
            <div className='container'>
                Thank You For Using Our Application!
            </div>
        </div>
    )
}

export default Logout;