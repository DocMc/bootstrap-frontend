import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import AuthenticationService from '../AuthenticationService/AuthenticationService';
import { useAuthContext  } from '../../../hooks/useAuthContext/useAuthContext';
import './Login.css'

const Login = () => {
    const [username, setUsername] = useState('DocMc');
    const [password, setPassword] = useState('');
    const [message, setMessage] = useState('');
    const nav = useNavigate();
    const { executeJwtAuthenticationService } = AuthenticationService()
    const {error} = useAuthContext();


    const loginCheck = async (e) => {
        let user = await executeJwtAuthenticationService(username, password);
        if(user){
            nav(`/welcome/${username}`);
        }
        else{
            setMessage(error);
        }
    }


    return (
        <div>
            <h1>Login</h1>
            <div className='container'>
                <div className={(message) ? 'alert alert-warning' : ''}>{message}</div>
                <div>
                    User Name: <input type='text' name='username' value={username} onChange={(e) => setUsername(e.target.value)} />
                    Password: <input type='password' name='password' value={password} onChange={(e) => setPassword(e.target.value)} />

                    <button className='btn btn-success' onClick={loginCheck}>
                        Login
                    </button>
                </div>
            </div>
        </div>
    )

}

export default Login