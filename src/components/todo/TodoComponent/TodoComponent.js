import React, { useState, useEffect, useCallback } from 'react';
import moment from 'moment';
import { ErrorMessage, Field, Form, Formik } from 'formik';

import AuthenticationService from '../AuthenticationService/AuthenticationService';
import TodoDataService from '../apis/todo/TodoDataService';
import { useParams, useNavigate } from 'react-router-dom';


const TodoComponent = ({ match, history }) => {
    //State
    const {id} = useParams();
    const nav = useNavigate()
    const { getLoggedInUserName } = AuthenticationService();


    const [_id] = useState(id);
    const [description, setDescription] = useState('');
    const [targetDate, setTargetDate] = useState(moment(new Date()).format('YYYY-MM-DD'));
    const [done, setHasCompleted] = useState(false);

    let descriptionHolder = description;
    let tagetDateHolder = targetDate;
    let completedHolder = done;

    const getTodo = useCallback ((username) => {
        TodoDataService.retrieveTodoById(username, _id)
            .then((r) => {
                setHasCompleted(r.data.done);
                setDescription(r.data.description);
                setTargetDate(moment(r.data.targetDate).format('YYYY-MM-DD'));
            }
            )
            .catch((e) => {
                console.log("Error: " + e);
            })
    }, [_id])

    useEffect(() => {
        if (id === -1) {
            return;
        }
        else {
            let username = getLoggedInUserName();
            getTodo(username);
        }
    }, [getTodo, getLoggedInUserName, id])


    const onFormSubmit = (values) => {
        let username = getLoggedInUserName();
        let todo = {
            id: id,
            description: values.description,
            targetDate: moment(values.targetDate).format('YYYY-MM-DD'),
            done: values.done
            
        }
        if (id === -1) {
            TodoDataService.createTodo(username, todo).then(() => {
                nav('/todos')
            })
        } else {
            TodoDataService.updateTodo(username, id, todo).then(() => {
                nav('/todos')
            })
        }
    }

    const validate = (values) => {
        let errors = {};
        if (!values.description) {
            errors.description = "Enter a Description";
        }
        else if (values.description.length < 5) {
            errors.description = "Enter at least 5 Characters in Description";
        }
        if (!moment(values.targetDate).isValid()) {
            errors.targetDate = 'Enter a valid Target Date';
        }
        return errors;
    }

    return (
        <div>
            <h1>Todo</h1>
            <div className='container'>
                <Formik
                    initialValues={{
                        description: descriptionHolder,
                        targetDate: tagetDateHolder,
                        done: completedHolder
                    }}
                    onSubmit={onFormSubmit}
                    validateOnChange={false}
                    validateOnBlur={false}
                    validate={validate}
                    enableReinitialize={true}>
                    {
                        ({ match }) => (
                            <Form>
                                <ErrorMessage name='description' component='div' className='alert alert-warning' />
                                <fieldset className='form-group'>
                                    <label>Description</label>
                                    <Field className='form-control' type='text' name='description' />
                                </fieldset>

                                <ErrorMessage name='targetDate' component='div' className='alert alert-warning' />
                                <fieldset className='form-group'>
                                    <label>Target Date</label>
                                    <Field className='form-control' type='date' name='targetDate' />
                                </fieldset>
                                <fieldset className='form-group'>
                                    <label>Completed
                                    <Field type='checkbox' name='done' />
                                    </label>
                                </fieldset>
                                <button type='submit' className='btn btn-success'>Save</button>
                            </Form>
                        )
                    }
                </Formik>
            </div>
        </div>
    )
}

export default TodoComponent;