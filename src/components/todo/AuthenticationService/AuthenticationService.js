import axios from "axios";
import { API_URL } from "../Constants/Constants";
import { useAuthContext } from "../../../hooks/useAuthContext/useAuthContext";

export const USER_NAME_SESSION_ATTRIBUTE_NAME = "authenticatedUser";
const AuthenticationService = () => {
    const { dispatch } = useAuthContext();

    const executeJwtAuthenticationService = async (username, password) => {
        try {
            let user = await axios.post(`${API_URL}/authenticate`, {
                username,
                password,
            });
            registerSuccessfulLoginForJWT(username, user.data.token);
            dispatch({ type: "LOGIN", payload: username });
            return user;
        } catch (err) {
            dispatch({type: "ERROR", payload: 'COULDN\'T REACH SERVER'})
        }
    };

    const createJWTToken = (token) => {
        return "Bearer " + token;
    };

    const registerSuccessfulLoginForJWT = (username, token) => {
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username);
        setupAxiosInterceptors(createJWTToken(token));
    };

    const logout = () => {
        sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        dispatch({type: 'LOGOUT'});
    };

    const isUserLoggedin = () => {
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        if (user === null) return false;
        return true;
    };

    const getLoggedInUserName = () => {
        let username = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        return username === null ? "" : username;
    };

    const setupAxiosInterceptors = (token) => {
        axios.interceptors.request.use((config) => {
            if (isUserLoggedin()) {
                config.headers.authorization = token;
            }
            return config;
        });
    };

    return {
        executeJwtAuthenticationService,
        createJWTToken,
        isUserLoggedin,
        setupAxiosInterceptors,
        logout,
        getLoggedInUserName,
    };
};

export default AuthenticationService;
