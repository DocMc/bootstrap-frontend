import React, { useState, useEffect, useCallback} from 'react';
import AuthenticationService from '../AuthenticationService/AuthenticationService';
import TodoDataService from '../apis/todo/TodoDataService';
import { useNavigate } from 'react-router-dom';

import moment from 'moment';

const ListTodos = () => {
    const nav = useNavigate();
    const { getLoggedInUserName } = AuthenticationService();
    const [todos, setTodos] = useState([]);

    const [message, setMessage] = useState(null);
    //retrieve the todo list
    

    //Refresh the todos list
    const refreshTodos = useCallback(() => {
        let getTodos = async () => {
        let username =  await getLoggedInUserName()
        TodoDataService.retrieveAllTodos(username)
            .then(
                (r) => {
                    setTodos(r.data);
                }
            )
        }
        getTodos();
    },[]);

    useEffect(() => {
        refreshTodos();
    }, [refreshTodos])

    const addTodoClick = () => {
        //let username = AuthenticationService.getLoggedInUserName();
        nav(`/todos/-1`)
    }

    //Delete Button onClick Call
    const deleteTodoClick = (id) => {
        let username = getLoggedInUserName();
        TodoDataService.deleteTodoById(username, id)
        .then((r) => {
            setMessage("Todo With ID: " + id + " Deleted")
            refreshTodos();
        })
    }

    const updateTodoClick = (id) => {
        nav(`/todos/${id}`)
    }

    //Make list
    const renderList = () => {
        if (todos !== null) {
            return todos.map((todo) => {
                return (
                    <tr key={todo.id}>
                        <td>{todo.description}</td>
                        <td>{moment(todo.targetDate).format('MM-DD-YYYY')}</td>
                        <td>{todo.done.toString()}</td>
                        <td><input value='Update' type='button'
                            className={todo.done ? 'invisible' : 'btn btn-success'}
                            onClick={() => updateTodoClick(todo.id)} />
                        </td>
                        <td><input value='Delete' type='button' onClick={() =>
                            deleteTodoClick(todo.id)}
                            className='btn btn-warning' />
                        </td>
                    </tr>
                )
            })
        }
        else {
            return ''
        }
    }

    //Render of Page
    return (
        <div>
            <h1>List Todos</h1>
            {message && <div className="alert alert-success">{message}</div>}
            <div className='container'>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Description</th>
                            <th>Target Date</th>
                            <th>Is Completed</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {renderList()}
                    </tbody>
                </table>
                <div className="row">
                    <button className='btn btn-success'  onClick={addTodoClick}>Add</button>
                </div>
            </div>
        </div>
    )
}

export default ListTodos;