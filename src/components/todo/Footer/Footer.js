import React from 'react';
import './footer.css';

const Footer = () => {
    return (
        <footer className='footer container'>
            <span className='text-muted'>All Rights Reserved 2021 @DocMc</span>
        </footer>
    )
}

export default Footer;