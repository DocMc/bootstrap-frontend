import {BrowserRouter as Router, Route, Routes, Navigate} from 'react-router-dom';

import Welcome from './components/todo/Welcome/Welcome';
import PageNotFound from './components/todo/PageNotFound/PageNotFound.js';
import Login from './components/todo/Login/Login';
import ListTodos from './components/todo/ListTodo/ListTodo';
import Header from './components/todo/Header/Header';
import Footer from './components/todo/Footer/Footer';
import Logout from './components/todo/Logout/Logout';
import TodoComponent from './components/todo/TodoComponent/TodoComponent';
import { useAuthContext } from './hooks/useAuthContext/useAuthContext';


const Todo = () => {
    const { user } = useAuthContext();
    return(
        <div className='TodoApp container'>
            <Router>
                <div>
                    <Header/>
                    <Routes>
                        <Route path="/" element={(user) ? <Navigate to={`/welcome/${user}`}/> : <Login/>} />
                        <Route path="/login" element={(user) ? <Navigate to={`/welcome/${user}`}/> : <Login/>} />
                        <Route path='/welcome/:name' element={user ? <Welcome/> : <Navigate to='/login'/>}/>
                        <Route path='/todos/:id' element={user ? <TodoComponent/> : <Navigate to='/login'/>} />
                        <Route path='/todos' element={user ? <ListTodos/> : <Navigate to='/login'/>} />
                        <Route path='/logout' element={user ? <Logout/> : <Navigate to='/login'/>} />
                        <Route element={<PageNotFound/>} />
                    </Routes>
                    <Footer/>
                </div>
            </Router>
        </div>
    )
}

export default Todo;